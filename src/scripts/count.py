#!/bin/python3

import json

def count_articles_no_incoherences():
    dataset = 'data/processed/articles-no-incoherences.json'
    print(dataset)
    d = dict()
    with open(dataset) as f:
        d = json.load(f)

    tot = 0
    count = dict()
    for media in d.keys():
        articles = d[media]["articles"]
        size = len(articles)
        count[media] = dict()
        count[media]["tot"] = size
        count[media]["male"] = 0
        count[media]["famale"] = 0
        tot += size
        for art in articles:
            if art["labels"]["target_gender"] == 0:
                count[media]["famale"] += 1
            else:
                count[media]["male"] += 1


    for media in count.keys():
        print(media)
        print(f"#art = {count[media]['tot']}")
        print(f"#M   = {count[media]['male']}")
        print(f"#F   = {count[media]['famale']}")
        print(f"%M   = {count[media]['male']/count[media]['tot'] * 100}")
        print(f"%F   = {count[media]['famale']/count[media]['tot'] * 100}")
    print(f"tot = {tot}")


def count_articles():
    dataset = "data/raw/articles.json"
    print(dataset)
    d = dict()
    with open(dataset) as f:
        d = json.load(f)

    tot = 0
    for media in d.keys():
        tot += len(d[media]["articles"])

    print(tot)


count_articles_no_incoherences()
count_articles()
