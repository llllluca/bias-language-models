#!/bin/python3

import json
import sys

def remove(dataset, field_name):
    
    same_field = dict()
    for media in dataset:
        articles = dataset[media]["articles"]
        for a in articles:
            field = a[field_name]
            if field not in same_field: 
                same_field[field] = []
            same_field[field].append({
                "media": media, 
                "article": a
            })

    new_dataset = dict()
    for field, articles in same_field.items():
        if len(articles) == 1:
            media = articles[0]["media"]
            article = articles[0]["article"]
            if media not in new_dataset:
                new_dataset[media] = {}
                new_dataset[media]["articles"] = []
            new_dataset[media]["articles"].append(article)

    return new_dataset


def main():
    if len(sys.argv) < 3:
        return

    dataset_input_path = sys.argv[1]
    dataset_output_path = sys.argv[2]

    dataset = dict()
    with open(dataset_input_path) as f:
        dataset = json.load(f)
    dataset = remove(dataset, "url")
    dataset = remove(dataset, "content")

    with open(dataset_output_path, "w") as f:
        f.write(json.dumps(dataset, indent=2, sort_keys=True))

    return


if __name__ == "__main__":
    main()

