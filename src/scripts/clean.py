import unicodedata
from multiprocessing import Pool
import re
import spacy
import sys
import json
from patterns import patterns
from stopwords import stopwords


def compile_regexs():
    regexs = list()
    #replace gendered words with person and non gendered pronouns
    for pattern, repl in patterns:
        regexs.append((re.compile(pattern), repl))

    emoticons =  re.compile("["
                   u"\U0001F600-\U0001F64F"  # emoticons
                   u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                   u"\U0001F680-\U0001F6FF"  # transport & map symbols
                   u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                   u"\U00002702-\U000027B0"
                   u"\U000024C2-\U0001F251"
                   u"\U0000200B"
                   u"\U0000202C"
                   u"\U00002069"
                   u"\U00002066"
                   u"\U0000202A"             
                   "]+", flags=re.UNICODE)
    regexs.append((emoticons, ''))

    #remove whole words from stop list 
    for word in stopwords: 
        pattern = '\\b' + word + '((-[a-zA-Z]*)|([,.!?;"\' ]))'
        regexs.append((re.compile(pattern), ""))

    various = [
        #remove numbers
        ('\d+', ''),
        #Cleanup the punctuation / symbols
        (' ,', ','),
        ('\+',  ''),
        ('&',   ''),
        ('  ',  ''),
        ('â€',  ''),
        ('ão',  ''),
        #replace huperson with human 
        #(an unintended consequence of our man/person regex swap)
        ('huperson',     'human'),
        ('ombudsperson', 'person'),
        ('ottoperson',   'norp')
    ]
    for pattern, repl in various:
        regexs.append((re.compile(pattern), repl))

    return regexs

def clean(raw_articles, media):
    print(f"STARTING {media.upper()}!")
    clean_articles = list()
    n = len(raw_articles)
    log_ratio = round(n/10)
    next_log = 0
    for i, a in enumerate(raw_articles):
        if i == next_log: 
            print(f"  {media}: {i+1}/{n}")
            next_log += log_ratio

        content = a["content"]
        content = unicodedata.normalize('NFKC', content)
        doc = nlp(content)
        for ent in reversed(doc.ents):
            if (ent.label_ == 'PERSON' or 
                ent.label_ == 'NORP' or 
                ent.label_ == 'GPE' or 
                ent.label == 'LOC'):
                content = content[:ent.start_char] + \
                    ent.label_.lower() + " " + \
                    content[ent.end_char:]

        for pattern, repl in regexs:
            content = re.sub(pattern, repl, content, re.IGNORECASE);

        a["content"] = content
        clean_articles.append(a)

    print(f"{media.upper()} COMPLETED!")
    return media, clean_articles


regexs = compile_regexs()
nlp = spacy.load("en_core_web_lg")
def main():
    if len(sys.argv) < 3:
        return

    raw_dataset_path = sys.argv[1]
    dataset_output_path = sys.argv[2]
    procmax = 1
    if len(sys.argv) >= 4:
        procmax = int(sys.argv[3])

    raw_dataset = dict()
    with open(raw_dataset_path, 'r') as f:
        raw_dataset = json.load(f)

    raw_data = list()
    for media in raw_dataset:
        raw_articles = raw_dataset[media]["articles"]
        raw_data.append((raw_articles, media))

    clean_dataset = dict()
    clean_data = list()
    with Pool(procmax) as pool:
        clean_data = pool.starmap(clean, raw_data)

    for media, clean_articles in clean_data:
        clean_dataset[media] = {}
        clean_dataset[media]["articles"] = clean_articles

    with open(dataset_output_path, "w") as f:
        f.write(json.dumps(clean_dataset, indent=2, sort_keys=True))


if __name__ == "__main__":
    main()

